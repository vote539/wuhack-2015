WUHack Web Site 2015
====================

This was built using Visual Studio.  For the best experience editing the project, I recommend also using Visual Studio, although it is not required.  I decided to use Visual Studio because I'd never used it for a web project before and I wanted to see how it worked for that application.  (My conclusion: overall Visual Studio is fine for this project, but since you do most of your debugging in the Web Inspector anyway, it's not all that much better than Sublime.)

To build the project outside of Visual Studio, I have set up a Grunt task.  This is how you can build the project for deployment on a server, for example.  First you need to install the dependencies:

    cd /path/to/wuhack-2015
    npm install
    npm install -g grunt-cli
    gem install compass

Then you can compile like this:

    grunt

The output directory *dist* will be created with all the compiled an minified sources.

It is sometimes a headache to get Compass to install correctly, in the right path and all.  Sorry about that.  Next time I'll use Less so it all stays inside the Node.JS ecosystem.

