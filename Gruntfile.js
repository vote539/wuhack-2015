﻿module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-mkdir");
    grunt.loadNpmTasks("grunt-contrib-compass");
    grunt.loadNpmTasks("grunt-typescript");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-uglify");

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        mkdir: {
            all: {
                options: {
                    mode: 0755,
                    create: ["dist", "dist/styles", "dist/scripts"]
                },
            },
        },
        compass: {
            main: {
                options: {
                    environment: "production",
		    outputStyle: "compress",
		    sassDir: "WUHack Web 2015/styles",
		    cssDir: "dist/styles"	    
                }
            },
            dev: {
                options: {
                    sassDir: "WUHack Web 2015/styles",
                    cssDir: "WUHack Web 2015/styles"       
                }
            }
        },
        typescript: {
            base: {
                src: ["WUHack Web 2015/scripts/*.ts"],
                dest: "WUHack Web 2015/scripts"
            }
        },
        uglify: {
            main: {
                files: {
                    "dist/scripts/main.js": ["WUHack Web 2015/scripts/main.js"]
                }
            }
        },
        copy: {
            dist: {
                cwd: "WUHack Web 2015",
                src: [
					"*.html",
					"images/**",
					".htaccess",
					"pdf/**"
                ],
                dest: "dist",
                expand: true
            }
        }
    });

    grunt.registerTask("default", [
		"mkdir",
		"compass",
		"typescript",
        "uglify",
		"copy"
    ]);

};